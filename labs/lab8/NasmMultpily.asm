; Izaak Miller
; NASM Lab 8
; This is equivalent to the C program: 
;   #include <stdio.h>
;   int main() {
;   int i = 36;
;   int j = 15;
;   int k = i*i + j*j;
;   printf("i=%d,j=%d,k=%d\n",i,j,k);
;   }

extern       printf     ; C's printf

section     .data       ; constants go here

i:       dd          36
j:       dd          15

fmt:     db        "i=%d, j=%d, k=%d", 10, 0

section     .bss        ;variables go here
k:      resb        4   ;space for k at the end
tp:     resb        4   ;space for a temp varialbe

section     .text       ;program goes here

global      main
main:
    push    ebp         ;always include theses two steps
    mov     ebp,esp     ;at the beginning of the program

    mov     eax,[i]     ;eax has contents of i
    mul     eax         ;eax multiplied by eax (36 * 36)
    mov     [tp],eax    ;temp now has contents of eax
    mov     eax,[j]     ;ebx has contents of j
    mul     eax         ;ebx multiplied by ebx (15 * 15)
    mov     ebx,[tp]    ;ebx has contents of temp variable
    add     eax,ebx     ;eax + temp  (36 * 36) + (15 * 15)
    mov     [k],eax     ;save contents of eax into k
    ; Get ready to print. We must push the arguments to the printf
    ; function in reverse order:

    push    dword   [k]     ;push i in first
    push    dword   [j]     ;push j in second
    push    dword   [i]     ;push k in last
    push    dword   fmt     ;then push the first argument (fmt)

    call    printf
    add     esp,16          ;since we pushed five words, pop 20 bytes

    mov     esp,ebp         ;always include these two steps
    pop     ebp             ;at the end

    ret                     ;return from the program



