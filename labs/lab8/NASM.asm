; Izaak Miller
; NASM Lab 8
; This is equivalent to the C program: 
;   #include <stdio.h>
;   int main() {
;   int r = 119;
;   int s = -32;
;   int t = 7;
;   int u = (r-(s-t)) & (s+t-r);
;   printf("r=%d,s=%d,t=%d,u=%d\n",r,s,t,u);
;   }

extern       printf     ; C's printf

section     .data       ; constants go here

r:       dd          119
s:       dd          -32
t:       dd          7

fmt:     db        "r=%d, s=%d, t=%d, u=%d", 10, 0

section     .bss        ;variables go here
u:      resb        4   ;space for u at the end
tp:     resb        4   ;temp

section     .text       ;program goes here

global      main
main:
    push    ebp         ;always include theses two steps
    mov     ebp,esp     ;at the beginning of the program

    mov     eax,[s]     ;eax has contents of s
    sub     eax,[t]     ;eax subtracted by t (-32 - 7)
    mov     ebx,[r]     ;ebx has contents of r
    sub     ebx,eax     ;ebx - eax (119 - -39)
    mov     [tp],ebx    ;save ebx into temp in order to carry out next part of operation
    mov     eax,[s]     ;eax has contents of s again
    add     eax,[t]     ;eax plus t (-32 + 7)
    sub     eax,[r]     ;eax subtracted by r (-25 -119)
    and     eax,[tp]    ;equivalent to:(s+t-r+) & (r-(s-t))
    mov     [u],eax     ;save contents of eax into u

    ; Get ready to print. We must push the arguments to the printf
    ; function in reverse order:

    push    dword   [u]     ;push u in first
    push    dword   [t]     ;push t in second
    push    dword   [s]     ;push s in third
    push    dword   [r]     ;push r in last
    push    dword   fmt     ;then push the first argument (fmt)

    call    printf
    add     esp,20          ;since we pushed five words, pop 20 bytes

    mov     esp,ebp         ;always include these two steps
    pop     ebp             ;at the end

    ret                     ;return from the program



