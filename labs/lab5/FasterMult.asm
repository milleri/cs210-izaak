# Izaak Miller
# Lab 5 
# October 5th, 2015
# This does "faster multiplication" of nonnegative integers a and b.
# It loops "a" times (a being the smaller integer out of the two integers)
# provided by the user.
# Honorcode: The work submitted is my own unless cited otherwise

	.data
prompt:	.asciiz	"Enter two non-negative integers to be multiplied, one per line: "

	.text
# Part 1 of program: read in the two integers and save them in registers s0, s1:
	la	$a0,prompt	# ask user to enter values
	li	$v0,4		# print_string
	syscall
	li	$v0,5		# read_int
	syscall
	add	$s0,$zero,$v0	# First integer a in s0
	li	$v0,5		# read_int
	syscall
	add	$s1,$zero,$v0	# Second integer b in s1
	
# Part 2 of program: compare the two integers and set smaller to register s2:

	slt	$s2,$s0,$s1	# Putting 1 in $s2 if $s0 is greater than $s1 if not set equal to 0 
	bne	$s2,$zero,loop	# if s0 < s1, do the "loop"
	add	$s2,$zero,$s0	# copy value of $s0 to $s2 (swapping values)
	add	$s0,$zero,$s1	# copy value of $s1 to $s0
	add	$s1,$zero,$s2	# copy value of $s2 to $s1
	
	
		
# Part 3 of program: use s2 to count down to 0 while adding b to sum:
	li 	$t0,0		# Sum in $t0
loop:	beq	$zero,$s0,done	# If a == 0, done
	add	$t0,$t0,$s1	# sum = sum + b
	addi	$s0,$s0,-1	# a = a-1
	j	loop
done:	add	$a0,$zero,$t0	# Copy sum into a0 for printing
	li	$v0,1		# print_int
	syscall
	
# Section 4: terminate program normally:
	li	$v0,10
	syscall
