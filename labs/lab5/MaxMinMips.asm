# Izaak Miller
# Lab 5 Part 2
# October 5th, 2015
# This finds the min and max of a user provided numbers of integers
# It loops "n" times (n being the number of integers provided by the user)
#
# Honorcode: The work submitted is my own unless cited otherwise

	.data
prompt:	.asciiz	"Enter an integer between 1-20: "
enter:	.asciiz	"Enter "
int:	.asciiz	" integers, one per line: "
nl:	.asciiz	"\n"
minMsg:	.asciiz "The minimum is "
maxMsg:	.asciiz	", and the maximum is "

	.text
# Part 1: Prompt the user to enter an integer between 1-20
	la	$a0,prompt	# Ask user to enter values
	li	$v0,4		# Print_string
	syscall
	li	$v0,5		# Read_int
	syscall
	add	$s0,$zero,$v0	# First integer n in s0
	la	$a0,enter	# Printing out enter
	li	$v0,4
	syscall	
	add	$a0,$zero,$s0	# Copy n into a0 for printing
	li	$v0,1		# Print_int
	syscall
	la	$a0,int		# Prompting user to enter "s0" integers
	li	$v0,4
	syscall
	
# Part 2: Calculate which integers are min and max
	add	$t0,$zero,$s0	# Setting up count value
	li	$v0,5		# Read_int
	syscall
	add	$s1,$zero,$v0	# Set first number equal to min and max
	add	$s2,$zero,$v0	# S1 and s2 are min and max
	addi	$t0,$t0,-1	# Subtracting count by 1
	
loop:	beq	$t0,$zero,done	# If count == 0 then jump to done and terminate
	li	$v0,5		# Read_int
	syscall
	slt	$s3,$s1,$v0	# Checking to see if $v0 is greater than $t1
	bne	$s3,$zero,max	# S1 is already min, check if new max
	add	$s1,$zero,$v0	# Setting t1 as new min in s1
max:	slt	$s3,$v0,$s2	# Checking to see if $t1 is greater than $s2
	bne	$s3,$zero,count	# $s2 is already max, decrease count by 1
	add	$s2,$zero,$v0	# Setting $v0 as new max in s2
count:	addi	$t0,$t0,-1	# Decrease count by 1
	j	loop		# Jump back to top of loop
	
# Part 3: Print out min and max
done:	la	$a0,minMsg		# Print statement for minimum
	li	$v0,4
	syscall
	add	$a0,$zero,$s1		# Printing out minimum value
	li	$v0,1
	syscall
	la	$a0,maxMsg		# Print statement for maximum
	li	$v0,4
	syscall
	add	$a0,$zero,$s2		# Printing out maximum value
	li	$v0,1
	syscall
	
# Part 4: Terminate the program normally
	li	$v0,10
	syscall