/**
 * Izaak Miller
 * Lab 2: Monday 14, 2015
 * Problem 3
 * mean.c:
 *   Demonstration of scanf. Finds arithmetic mean of two numbers
 *   To compile and run:
 *
 *     gcc mean.c  -o mean
 *     ./mean
 *
 * Honor code: The work that I am submitting is my own unless otherwise cited.
 */

#include <stdio.h>
//prototypes
double mean(int i, int j);

int main() {
    int i, j;
    double m;

    printf("Enter two integer values: ");
    scanf("%d %d", &i, &j);
    m = mean(i,j);
    printf("\nThe arithmetic mean of %d and %d is %.1lf. \n", i, j, m); //making 1 demical place


}
double mean(int i, int j){ //getting mean of two numbers
    double int1 = i;
    double int2 = j;
    double mean = (i + j) / 2;
    return mean;
}
