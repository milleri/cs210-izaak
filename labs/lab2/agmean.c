/**
 * Izaak Miller
 * Lab 2: Monday 14, 2015
 * Problem 5
 * agmean.c:
 *   Demonstration of scanf. Finds arithmetic mean of two numbers
 *   To compile and run:
 *
 *     gcc agmean.c -lm -o agmean
 *     ./agmean
 *
 * Honor code: The work that I am submitting is my own unless otherwise cited.
 */

#include <stdio.h>
#include <math.h>

//prototypes
int even(int x, int y);
double agmean(int w, int z);

int main(){

unsigned value1, value2;
double answer;

printf("Enter two integers: ");
scanf("%u %u", &value1, &value2);
answer = agmean(value1, value2); //setting answer equal to agmean
printf("\nAGmean(%u, %u) is %.3lf.\n", value1, value2, answer); //printing results

}//main

int even(int x, int y){ //checking if even
    int v1 = x;
    int v2 = y;

    if((v1 + v2)%2 == 0){
        return 1;
    }//if

    else if((v1 + v2)%2 != 0){
        return 0;
    }

}//even

double agmean(int w, int z){ //getting mean of two values
    int valueOne = w;
    int valueTwo = z;
    int test = even(valueOne, valueTwo);

    if(even(w,z)){
    double mean = (w + z) / 2;
    return mean;
    }//if

    else{
    double agmean = sqrt(w*z) / 2;
    return agmean;


    }//if

}//agmean
