/**
 * Izaak Miller
 * Lab 2: Monday 14, 2015
 * Problem 6
 * PigLatin.c:
 *
 * Purpose: Manipulate a string up to twenty characters long and
 *  translate the string into piglatin
 *
 *     gcc PigLatin.c -o PigLatin
 *     ./agmean
 *
 * Honor code: The work that I am submitting is my own unless otherwise cited.o
 *
 * Some inspiration was from the chars.c that was provided to us from Bob Roos's Website and on the Ubuntuforums.org
 */

#include <stdio.h>
//prototypes
int size(char* word1);
void pigLatin(char String1[], char String2[]);

int main(){
    //leaving room for 'a' and 'y' at the end of the string
    char String1[7] = "Cool";
    char String2[11] = "Computer";
    char String3[6] = "Lab";
    char String4[7] = "Cool";
    char String5[11] = "Computer";
    char String6[6] = "Lab";
//created 6 strings to avoid having to 'fix'the strings


    printf("The original 3 strings are \n%s \n%s \n%s \n",String1, String2, String3); //Printing original words
    pigLatin(String1, String2); //Manipulating words with pigLatin
    pigLatin(String3, String4);
    pigLatin(String5, String6);

    printf("\nThe first two strings manipulated with pigLatin are %sy and %sy \n", String1, String2); //Printing results
    printf("\nThe first and third strings manipulated with pigLatin are %sy and %sy \n", String3, String4);
    printf("\nThe second and third strings manipulated with pigLatin are %sy and %sy \n", String5, String6);
}

int size(char* word) {
     int i = 0;
     while (word[i] != '\0') { //removing null character from size
         ++i;
     } // getting size of array
     return i; //return size
}

void pigLatin(char* String1, char* String2){

     char head1[1];
     char head2[1];
     int i = 1;
     int j = 1;

     head1[0] = String1[0]; //getting first character of string
     head2[0] = String2[0];

     while(String1[i] != '\0'){ //shifting all characters left by one
         String1[i - 1] = String1[i];
         i++;
     }
     while(String2[j] != '\0'){
         String2[j - 1] = String2[j];
         j++;
     }

     String1[size(String1) - 1] = head2[0]; //setting first character of other string to the back of string
     String2[size(String2) - 1] = head1[0];

     String1[size(String1)] = 'a'; //adding "ay" to back of string to translate to pigLatin
    // String1[size(String1)] = 'y';

     String2[size(String2)] = 'a';
     //String2[size(String2)] = 'y';


     //Had a difficult time trying to add the y at the end of the string.
     //I believe that the null character was probably causing me the issue
     //but I couldn't really figure it out
     //So I simplified it.
     //I still manipulated the strings however.

}

