#Izaak Miller
#Lab 3 September 21, 2015
#Doing the calcualation:
#	x = (a - b)*3 + (a + b)*6;
#Honor code: The work I am submitting is my work unless otherwise cited

	.data
	.align 2 
a:	.word 10 #4 bytes of memory containing integer 10
b:	.word -1 #4 bytes of memory containing integer -1
x:	.space 4 #4 bytes of memory

	.text
	lw	$t0,a		#loading contents from memory a to $t0
	lw	$t1,b		#loading contents from memory b to $t1
	sub	$t0,$t0,$t1	#subtracting b from a
	add	$t1,$zero,$t0	#copying $t0 into $t1
	add	$t0,$t0,$t0	#'Multiplying' (a - b)*3
	add	$t0,$t1,$t0	# 	^ ^
	lw	$t1,a		#loading contents from memory a to $t1
	lw	$t2,b		#loading contents from memory b to $t2
	add	$t1,$t1,$t2	#adding a + b
	add	$t1,$t1,$t1	#adding (a + b)*2
	add	$t2,$zero,$t1	#copying $t1 to $t2
	add	$t1,$t1,$t1	#adding (a + b)*4
	add	$t1,$t1,$t2	#adding (a + b)*6	*what we wanted*
	add	$t0,$t0,$t1	#adding (a - b)*3 + (a + b)*6	*Final result*
	sw	$t0,x		#store result in x
	
	li	$v0,10
	syscall			#ends the program
	
