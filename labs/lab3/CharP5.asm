# Izaak Miler
# Lab 3: September 21, 2015
# Example 5: Entering following code and noting what value goes into register $t0

	.data
	.align	2
letter:	.ascii	"A"
pos	:.word	65	#four byte of memory containing the integer 65
c:	.space	4	#four byte of memory containing an integer between 0 - 25
	.text
	
	lw	$t0,letter	#loading contents from memory letter to $t0
	add	$t1,$t0,$zero	#copying contents from $t0 to $t1
	lw	$t0,pos		#loading contents from memory pos to $t0
	sub	$t0,$t1,$t0	#subtracting $t0 from $t1 to get place position of alphabet
	sw	$t0,c		#saving position in memory c should be between 0 - 25
	
	li	$v0,10		#ending mips
	syscall
