#Izaak Miller
#Lab 3 September 21, 2015
#Doing the calcualation:
#	x = 7*y;
#Honor code: The work I am submitting is my work unless otherwise cited

	.data
	.align 2 
y:	.word 13 #4 bytes of memory containing integer 13
x:	.space 4 #4 bytes of memory

	.text
	lw	$t0,y		#loading contents from memory y to $t0
	add	$t0,$t0, $t0	# 2 * y
	add	$t0,$t0, $t0	# 4 * y
	add	$t0,$t0, $t0	# 8 * y
	lw 	$t1,y		#loading contents from memory y to $t0
	sub	$t0,$t0,$t1	#subtracting y from (8 * y) = 7 * y
	sw	$t0,x
	
	li	$v0,10		#ending MIPS
	syscall