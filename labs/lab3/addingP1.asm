#Izaak Miller
#Lab 3 September 21, 2015
#Doing the calcualation:
#	x = a - (2*b + c - d);
#Honor code: The work I am submitting is my work unless otherwise cited

	.data
	.align 2 
a:	.word 10 #4 bytes of memory containing integer 10
b:	.word 20 #4 bytes of memory containing integer 20
c:	.word -30 #4 bytes of memory containing integer -30
d:	.word -40 #4 bytes of memory containing integer -40
x:	.space 4 #4 bytes of memory

	.text
	lw 	$t0,b		#loading contents from memory b into $t0
	add	$t0,$t0,$t0	#adding b + b (b*2)
	lw	$t1,c		#loading contents from memory c into $t1
	add	$t0,$t0,$t1 	#adding (2*b) + c
	lw	$t1,d		#loading contents from memory d into $t1
	sub	$t0,$t0,$t1	#subtracting d from (2*b + c)
	lw	$t1,a		#loading contents from memory a into $t1
	sub	$t0,$t1,$t0	#subtracting (2*b + c) -d  from a
	sw	$t0,x		#storing result into memory x
	
	li	$v0,10
	syscall			#ends the program

