/**
 * Izaak Miller
 * Due October 18, 2015
 * Lab 6, part 1
 * Purpose: This program shifts three arrays to the left one position
 * and prints out the new shifted (rotated) array.
 *
 * Honor code: The work submitted is my own unless otherwise cited
 *
 */
#include <stdio.h>

/* Function prototypes: */
void aprint(int a[], int n);
void rotl(int a[], int n);

int main() {
  /* Three arrays to be rotated: */
  int a1[] = {10,20,30,40};
  int a2[] = {3};
  int a3[] = {11,12,13,14,15,16,17,18,19};

  /* Array a1: */
  printf("Unrotated array: ");
  aprint(a1,4);
  rotl(a1,4);
  printf("Rotated array: ");
  aprint(a1,4);
  rotl(a1,4);

  /* Array a2: */
  printf("Unrotated array: ");
  aprint(a2,1);
  rotl(a2,1);
  printf("Rotated array: ");
  aprint(a2,1);
  rotl(a2,1);

  /* Array a3: */
  printf("Unrotated array: ");
  aprint(a3,9);
  rotl(a3,9);
  printf("Rotated array: ");
  aprint(a3,9);
  rotl(a3,9);
}

/**
 * aprint(a,n): prints the elements of array a (of size n)
 *   on a single line, separated by blanks and terminating
 *   with a newline.
 */
void aprint(int a[], int n) {
  int i;

  for (i = 0; i < n; i++) {
    printf("%d ",a[i]);
  }
  printf("\n");
}

/**
 * rotl description goes here
 */
void rotl(int a[], int n) {
    int i;
    int b[n];
    int temp;
    temp = a[0];

   for(i = 0; i < n - 1; i++){
        a[i] = a[i+1];
    } /** for */
    a[n-1] = temp;
}
