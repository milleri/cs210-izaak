# Izaak Miller
# Due: October 18th, 2015 
# Lab 6, part 2
# Program rotates the array to the left. Added code that would move every number left one place.
# Honor code: The work is my own unless otherwise cited
	.data
	.align	2
a1:	.word	10,20,30,40	# the array to be rotated
lab1:	.asciiz	"Unrotated array: "
lab2:	.asciiz	"Rotated array: "
space:	.asciiz	" "
nl:	.asciiz	"\n"

	.text
	la	$a0,lab1	# Print label for unrotated array
	li	$v0,4
	syscall
	
	la	$a0,a1		# Print the unrotated array
	li	$a1,4
	jal	aprint
	
#########################################################
# Rotating array

	li	$s2,4		# Setting counter to 3(counting down from 3)
	la	$t0,a1		# Loading address of array
	lw	$t1,0($t0)	# t1 is equal to first number in array (10)
	
check:	beq	$zero,$s2,stop	# Branching if counter equals zero (Array is rotated)
	addi	$t0,$t0,4	# Add 4 to address to get next index of array
	lw	$t2,0($t0)	# Load next index into t2
	addi 	$t0,$t0,-4	# Subtract 4 to go back to original address
	sw	$t2,0($t0)	# Store new value one spot ahead of previous index
	addi	$t0,$t0,4	# Move ahead one index in array for next rotation
	
	addi	$s2,$s2,-1	# Subtracting one from the counter
	j	check
	
stop:	addi	$t0,$t0,-4	# Last index in array
	sw	$t1,0($t0)	# Put first number saved from before in last spot in array

########################################################
	
	la	$a0,lab2	# Print label for rotated array
	li	$v0,4
	syscall
	
	la	$a0,a1		# Print the rotated array
	li	$a1,4
	jal	aprint
		
	li	$v0,10		# Terminate program
	syscall	

#########################################################
############ Function aprint:
############    prints array at address a0 of length a1
#########################################################

aprint:	addi	$sp,$sp,-8	# Save $s0 and $s1 on the stack
	sw	$s0,0($sp)
	sw	$s1,4($sp)
	
	move	$s0,$a0		# Save a0 in s0--we need a0 for syscalls
	li	$s1,0		# s1 is our loop counter
	
loop:	slt	$t0,$s1,$a1	# See if we're done yet
	beq	$t0,$zero,done	# exit loop if s1 >= a1 (i.e., if counter >= array size)
	lw	$a0,0($s0)	# Print next array element:
	li	$v0,1
	syscall
	
	la	$a0,space	# Print a space after the array element:
	li	$v0,4
	syscall
	
	addi	$s0,$s0,4	# Move forward to next array value (4 bytes per int)
	addi	$s1,$s1,1	# Increment the loop counter
	j	loop
	
done:	la	$a0,nl		# Print a newline:
	li	$v0,4
	syscall
	
	lw	$s0,0($sp)	# Restore the old values of s0, s1
	lw	$s1,4($sp)
	addi	$sp,$sp,8
	
	jr	$ra		# Finished--return to caller
