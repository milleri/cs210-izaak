# Izaak Miller   Lab 4
# Due: September 28, 2015
# Honorcode: The work I am submitting is my own work unless cited otherwise
#
# Problem 3: 
# Creating a program that does a variety of operations 
# with two integers
#
# For variety, the first input value is saved in memory;
# the second one is merely copied into a temporary register.

        .data
        .align     2      			# get to a word boundary
x:      .space     4      			# space for an integer
y:	.space	   4				# space for an integer
quote:	.asciiz	 "Enter two integers: \n"	# Prompting user
AND:    .asciiz  "\nAND: "     			# print before AND operation for neatness
OR:	.asciiz	 "\nOR: "			# print before OR operation for neatness
XOR:	.asciiz	 "\nXOR: "			# print before XOR operation for neatness
SLLx:	.asciiz	 "\nSLL for X: "		# print before SLLx operation for neatness
SRLx:	.asciiz	 "\nSRL for X: "		# print before SRLx operation for neatness
SRAx:	.asciiz	 "\nSRA for X: "		# print before SRAx operation for neatness
SLLy:	.asciiz	 "\nSLL for Y: "		# print before SLLy operation for neatness
SRLy:	.asciiz	 "\nSRL for Y: "		# print before SRLy operation for neatness
SRAy:	.asciiz	 "\nSRA for Y: "		# print before SRAy operation for neatness

        .text
        
        # prompting user
        la       $a0,quote	# load address
        li       $v0,4		# code for print_string
        syscall			# peform print out
        
        # saving integers
	li	$v0,5    # code for read_int
        syscall		 # perform the read
        sw	$v0,x	 # save it in memory x
        
        li 	$v0,5	 # code for read_int
        syscall		 # perform the read
        sw	$v0,y	 # save it in memory y
        
        # logical AND of x and y
        la	$a0,AND		# load address AND into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        lw	$t0,x		# load address x into $t0
        lw 	$t1,y		# load address y into $t1
        and	$a0,$t0,$t1	# Operation: AND of x and y
        li	$v0,1		# code for print_int
        syscall			# perform print out
        
        # logical OR of x and y
        la	$a0,OR		# load address OR into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        or	$a0,$t0,$t1	# Operation: OR of x and y
        li	$v0,1		# code for print_int
        syscall			# perform print out
                
        # logical XOR of x and y
        la	$a0,XOR		# load address XOR into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        xor	$a0,$t0,$t1	# Operation: XOR of x and y
        li	$v0,1		# code for print_int
        syscall			# perform print out
                
        # logical SLL of x
        la	$a0,SLLx	# load address SLLx into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        sll	$a0,$t0,2	# Operation: SLL of x
        li	$v0,1		# code for print_int
        syscall			# perform print out

        # logical SLL of y
        la	$a0,SLLy	# load address SLLy into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        sll	$a0,$t1,2	# Operation: SLL of y
        li	$v0,1		# code for print_int
        syscall			# perform print out

        # logical SRL of x
        la	$a0,SRLx	# load address SRLx into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        srl	$a0,$t0,2	# Operation: SRL of x
        li	$v0,1		# code for print_int
        syscall			# perform print out
        
        # logical SRL of y
        la	$a0,SRLy	# load address SRLy into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        srl	$a0,$t1,2	# Operation: SRL of y
        li	$v0,1		# code for print_int
        syscall			# perform print out
        
        # logical SRA of x
        la	$a0,SRAx	# load address SRAx into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        sra	$a0,$t0,2	# Operation: SRA of x
        li	$v0,1		# code for print_int
        syscall			# perform print out

        # logical SRA of y
        la	$a0,SRAy	# load address SRAy into $a0
        li	$v0,4		# code for print_string
        syscall			# perform print out
        sra	$a0,$t1,2	# Operation: SRA of y
        li	$v0,1		# code for print_int
        syscall			# perform print out

	# Terminate program
        li       $v0,10   # code for system exit
        syscall           # terminate program
