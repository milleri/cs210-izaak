/**
 * Izaak Miller
 * Lab 1
 * Sample program (based on K & R page 17):
 * Copy a file, count vowels and consonants
 *
 * To compile and run this on a data file named "myfile.txt", type:
 *
 *      gcc CountingVowelsAndConsonants  -o CountingVowelsAndConsonants
 *      ./CountingVowelsAndConsonants  <  myfile.txt
 */
#include <stdio.h>
int main() {
    int c;
    int vcount = 0;
    int ccount = 0;

    while ((c = getchar()) != EOF) {


        if ((c > 'a' && c < 'e') || (c > 'e' && c < 'i') || (c > 'i' && c < 'o') || (c > 'o' && c < 'u') || (c > 'u' && c < 'z')){
            ccount++;
        }

        else if ((c > 'A' && c < 'E') || (c > 'E' && c < 'I') || (c > 'I' && c < 'O') || (c > 'O' && c < 'U') || (c > 'U' && c < 'Z')){
            ccount++;
        }

        else if ((c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u') || (c == 'A') || (c == 'E') || (c == 'I') || (c == 'O') || (c == 'U')) {
            vcount++;
        }

    }

    printf("Number of vowels: %d \nNumber of consonants: %d\n ", vcount, ccount);
}
