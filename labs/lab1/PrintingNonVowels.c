/**
 * Izaak Miller
 * Lab 1
 * Sample program (based on K & R page 17):
 * Copy a file, printing only the non vowel characters
 * (not including \n, which is always copied).
 *
 * To compile and run this on a data file named "myfile.txt", type:
 *
 *      gcc PrintingNonVowels.c  -o PrintingNonVowels
 *      ./PrintingNonVowels  <  myfile.txt
 */
#include <stdio.h>
int main() {
    int c;
    while ((c = getchar()) != EOF) {
        if (c == '\n') {
            putchar(c); /* Don't count the newline! */
        }
        else if (c != 'a' && c != 'e' && c != 'i' && c != 'o' && c != 'u') {
        putchar(c); /**Print character if not vowel*/
        }
    }
}
