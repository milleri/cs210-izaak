/**
 * Izaak Miller
 * Lab 1
 * Sample program (based on K & R page 17):
 * Counting lines
 *
 * To compile and run this on a data file named "myfile.txt", type:
 *
 *      gcc Countinglines.c  -o Countinglines
 *      ./Countinglines  <  myfile.txt
 */
#include <stdio.h>
int main() {
    int c, nl;
    nl = 1;

    printf("%3d. ", nl);

    while ((c = getchar()) != EOF) {
        if (c == '\n') {
            nl++;/*Counts the newline! */
            printf("\n%3d. ", nl);
        }//if

        else if(c != '\n'){
        putchar(c);
        }//if

    }//while

    printf("END OF TEXT FILE \n");
}//main
