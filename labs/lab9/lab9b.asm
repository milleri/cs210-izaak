# Izaak Miller
# Lab 9 Part b: 
# Purpose: Use function f to calculate (w & (x/4)) | (y & 37)
# November 16th, 2015
# Honor code: The work submitted is my own unless cited otherwise


	.data
	.align	2
w:	.word	77		# See problem description
x:	.word	-14		# See problem description
y:	.word	89		# See problem description

	.text
	li	$s0,9999	# Dummy value to test that s0 gets restored
	li	$s1,-99999	# Dummy value to test that s1 gets restored
	lw	$a0,w		# Pass w to f in register a0
	lw	$a1,x		# Pass x to f in register a1
	lw	$a2,y		# Pass y to f in register a2
	jal	f		# Call function f
	
	move	$a0,$v0		# Print the result returned from f in register v0
	li	$v0,1
	syscall
	
	li	$v0,10		# Normal program termination
	syscall

	
f:	sra	$a1,$a1,2	# Same as dividing by 4
	and	$a0,$a0,$a1	# And operation: w & (x/4)
	andi	$a2,$a2,37	# And operation: y & 37
	or	$v0,$a0,$a2	# Or operation: (w & (x/4)) | (y & 37)
	jr	$ra		# Return to caller
