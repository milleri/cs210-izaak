/* Izaak Miller
 * Lab 9 Part 2B
 * November 16th, 2015
 * Purpose: replaces each element in the parameter
 * array a with the sum of the two elements on either side of it
 */

#include <stdio.h>

/* Function prototype: */
void arrayCalc(int a[], int len);

int main() {
    int a1[] = {4,2,9,-1,6,3};
    int a2[] = {10,9,11,12,15,28,19};
    int a3[] = {2,3,5,8,0,10,1};
    int l1 = 6, l2 = 7, l3 = 7, i;
    arrayCalc(a1,l1);
    for (i = 0; i < l1; i++) { printf("%d ",a1[i]); }
    printf("\n");
    arrayCalc(a2,l2);
    for (i = 0; i < l2; i++) { printf("%d ",a2[i]); }
    printf("\n");
    arrayCalc(a3,l3);
    for (i = 0; i < l3; i++) { printf("%d ",a3[i]); }
    printf("\n");
}


void arrayCalc(int a[], int len) {
    int i,temp, temp2, temp3;
    int firstNum = a[0] + a[1];
    for(i = 0; i < len-1; i++){
        temp = a[i];
        temp2 = a[i+1];
        a[i] = temp3 + temp2;
        temp3 = temp;
    }
    a[0] = firstNum;
    a[len-1] = temp2 + temp;

}

