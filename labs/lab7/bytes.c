/**
 * Izaak Miller
 * Lab 7
 * October 26th, 2015
 * Honor code: The work that I am submitting is my own
 * Purpose: display number of bytes in each data type
 * */

#include <stdio.h>

int main() {

    /** Uses sizeof() to get size of data types then prints them*/
    printf("Number of bytes for int: %lu \n bytes", sizeof(int));
    printf("Number of bytes for float: %lu \n bytes", sizeof(float));
    printf("Number of bytes for double: %lu \n bytes", sizeof(double));
    printf("Number of bytes for char: %lu \n bytes", sizeof(char));
    printf("Number of bytes for short: %lu \n bytes", sizeof(short));
    printf("Number of bytes for long: %lu \n bytes", sizeof(long));
    printf("Number of bytes for long long: %lu \n bytes", sizeof(long long));

}
