/**
 * Izaak Miller
 * Lab 7
 * October 26th, 2015
 *
 * Honor code: The work submitted is my own unless cited
 *
 * Using bit patterns as masks. This program takes four characters
 * and, using shift operators, stores them in the four bytes of an
 * unsigned int.
 *
 * Then it uses "masks" and shift operators to extract each of the bytes
 * and print them back out as characters.
 */
#include <stdio.h>

int main() {
    unsigned int mask = 0xFFFFFFFF;  /** mask value set to all 1's */
    unsigned int a;
    int beginning, end;

    printf("Enter an unsigned integer a: ");    /** Asks user for unsignged integer */
    scanf("%u",&a);                             /** Saves value into unsigned int a */
    printf("\nEnter beginning and ending bit positions (between 0 and 31): "); /** Asks user for beginning and ending values */
    scanf("%d %d",&beginning, &end);            /** Saves value of beginning and end */

    mask = (mask << beginning);                 /** Shifts the mask value left by beginning */
    mask = (mask >> beginning);                 /** Shifts the mask value back to original position */

    a = (a & mask) >> (31-end);                 /** Shifts the mask value left and masks the a value */
    printf("Answer = %u\n",a);                  /** Prints out the new a value */
    }
