/**********************************************************
 *  Izaak Miller
 *  Final Project
 *  December 11th, 2015
 *  Purpose: Take math expression entered by user and
 *  print out correct MIPS instructions
 *
 *  gcc mipsTranslator.c - o mipsTranslator
 *  ./mipstranslator
 *
 **********************************************************/




#include <stdio.h>
#include <string.h>
#define expression 82
#define STACKMAX 100

/* Function Prototypes: */
void push(int stack[], int *top, int value);
int pop(int stack[], int *top);

int size(char *s);
void add(int num2);
void sub(int num2);
void mult(int num2);
void div(int num2);

 int main() {

     /* Variables and Declarations */
     char read[expression];     /** Save the expression into array read with length 82*/
     int len; /* length of expression*/
     int digits[STACKMAX]; /* Store digits from expression */
     int digitsTop = -1;
     int operators[STACKMAX]; /* Store operators */

     int checknum1 = 1;
     int num1;
     int num2 = 0;
     int num3 = 0;
     char op;
     int  count = 1;
     int  integer1 = 0;
     int  integer2;
     int f = 0;


     printf("Enter an integer expression:\n");
     fgets(read,expression,stdin);
     read[strlen(read)-1] = '\0'; /* get rid of new line */
     printf("Paste the following code into MARS:\n");

     printf("    .text\n");

     len = size(read); //length of expression

     int c = 0;
    integer1 = read[0] - '0'; /** Convert char into int*/
    num1 = integer1;
    while(checknum1){
        c++;
         if((read[c] == '0' || read[c] > '0') && (read[c] == '9' || read[c] < '9')){
        integer1 = read[c] - '0';
        num1 = num1 * 10;
        num1 = num1 + integer1;
        }
        else{
        checknum1 = 0;
         printf("    li      $s0,%d\n",num1);
        }
    }

    int i;
    for (i = 1; i < len; i++) {
        if(read[i] == '+' || read[i] == '-' || read[i] == '*' || read[i] == '/'){
            op = read[i];
            count = 1;
        }
        else if((read[i] == '0' || read[i] > '0') && (read[i] == '9' || read[i] < '9')){
                integer2 = read[i] - '0';
                num2 = integer2;
            while(count){ /**While loop builds integers*/
                i++;
                if((read[i] == '0' || read[i] > '0') && (read[i] == '9' || read[i] < '9')){
                integer2 = read[i] - '0';
                num2 = num2 * 10;
                num2 = num2 + integer2;
                }
                else{
                    count = 0;
                }

            }
            if (op == '*'){
                mult(num2);
            }
            else if(op == '/'){
                div(num2);
            }
            else if(op == '+'){
                /**int j;
                for(j = i; j < len; j++){
                    if((read[j] == '0' || read[j] > '0') && (read[j] == '9' || read[j] < '9')){
                        integer2 = read[j] - '0';
                        num2 = integer2;
                        count = 1;
                    while(count){
                    j++;
                    if((read[j] == '0' || read[j] > '0') && (read[j]  == '9' || read[j] < '9')){
                        integer2 = read[j] - '0';
                        num2 = num2 * 10;
                        num2 = num2 + integer2;
                    }
                    else{
                        count = 0;
                         }
                     }
                    }
                    else if(read[j] == '*'){
                        int k;
                        f = 0;
                        for(k = j; k < len; k++){
                            if((read[k] == '0' || read[k] > '0') && (read[k] == '9' || read[k] < '9')){
                                integer2 = read[k] - '0';
                                num3 = integer2;
                                count = 1;
                                while(count){
                                k++;
                                if((read[k] == '0' || read[k] > '0') && (read[k] == '9' || read[k] < '9')){
                                    integer2 = read[k] - '0';
                                    num3 = num3 * 10;
                                    num3 = num3 + integer2;
                                }
                                else{
                                count = 0;
                                printf("    li      $s2,%d\n", num2);
                                printf("    li      $s3,%d\n", num3);
                                printf("    mult    $s3,$s2\n");
                                printf("    move    $s3,$s1\n ");
                                f++;
                                    if(f > 2){
                                        printf("    mult    $s1,$s3\n");
                                    }
                                } /** Change the repositories so that $s1 does not keep being replaced. Also copy the format from addition into subtraction*/
                           /*  }
                            }
                        }
                        i = k;
                        //printf("    li      $s1,%d\n", num2);
                        //printf("    li      $s2,%d\n", num3);
                        //printf("    mult    $s1,$s2\n");
                         }
                    }*/
                add(num2);
                }
            }
            else if(op == '-'){
                sub(num2);
            }
        } /* Order of operations with if statements */
    }


/**
 * size: returns the length of the string s
 */
int size(char *s) {
     int i = 0;
     while (s[i] != '\0') {
         ++i;
     }
     return i;
}

/** Adding function */

void add(int num) {
    int num2 = num;
    printf("    li      $s1,%d\n", num2);
    printf("    add     $s0,$s0,$s1\n");
}

/** Subtracting function */

void sub(int num) {
    int num2 = num;
    printf("    li      $s1,%d\n", num2);
    printf("    sub     $s0,$s0,$s1\n");
}

/** Multiplying function */

void mult(int num) {
    int num2 = num;
    printf("    li      $s1,%d\n", num2);
    printf("    mult    $s0,$s1\n");
}

/** Division function */

void div(int num) {
    int num2 = num;
    printf("    li      $s1,%d\n", num2);
    printf("    div     $s0,$s1\n");
}

