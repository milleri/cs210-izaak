//Draws a diamond
//Created by Dan B. and Izaak M.
//Incomplete
#include <stdio.h>
int main(){
    char star = '*', space = ' ';
    int row, col;

    for(row = 0; row < 7; row++){
        for (col = 0; col < 7; col++){
            if (row%2 == 0){
                printf("%c", star);
            }
            else {
                printf("%c", space);
            }
        }
        printf("\n");

    }

}
